<?php

/**
 * @file
 * Tests for crop_entity.module.
 */

use Drupal\crop_entity\CropEntity;
use Drupal\crop_entity\CropEntityUtility;

/**
 * Provides methods specifically for testing Media module's field handling.
 */
class CropEntityTestCase extends DrupalWebTestCase {
  protected $adminUser;
  protected $node;
  protected $mediaFileFieldTest;
  protected $referenceImage;
  protected $originalImage;
  protected $contentType;
  protected $crop1;
  protected $crop2;

  /**
   * Tell the system who we are.
   */
  public static function getInfo() {
    return array(
      'name' => 'Crop Entity',
      'description' => 'Functional tests Crop Entity',
      'group' => 'Crop Entity',
    );
  }

  /**
   * Setup test af crop.
   */
  public function setUp() {
    $modules[] = 'media';
    $modules[] = 'crop_entity';
    $modules[] = 'crop_entity_file_type';
    parent::setUp($modules);
    $this->admin_user = $this->drupalCreateUser(array(
      'access content',
      'view files',
      'view own files',
      'access media browser',
      'access administration pages',
      'administer site configuration',
      'administer users',
      'administer permissions',
      'administer content types',
      'administer nodes',
      'administer files',
      'bypass node access',
      'bypass file access',
    ));

    $this->drupalLogin($this->admin_user);
    $module_path = drupal_get_path('module', 'crop_entity');
    $this->contentType = $this->drupalCreateContentType();
    $this->originalImage = $this->createImageEntity('cropped_image.jpg');
    $this->referenceImage = realpath($module_path . '/test/test_image_crop.jpg');

    $this->attachFileField('field_image', 'node', $this->contentType->type);

    $node = array(
      'title' => 'Image crop test article',
      'type' => $this->contentType->type,
      'field_body' => array(LANGUAGE_NONE => array(array('value' => 'THIS IS THE BODY TEXT'))),
      'field_image' => array(LANGUAGE_NONE => array(array('fid' => $this->originalImage->fid))),
    );
    $this->node = $this->drupalCreateNode($node);
  }

  /**
   * Execute tests.
   */
  public function testAll() {
    $this->doTestEmptyCropWidget();
    $this->doTestCropFunction();
    $this->doTestImageReplacement();
    $this->doTestPopulatedCropWidget();
  }

  /**
   * Test that the original image is cropped according to the crop values.
   */
  protected function doTestCropFunction() {
    $this->crop1 = CropEntityUtility::buildCropEntity($this->originalImage, '16:9', 0, 0, 800, 450);
    $crop_handler = new CropEntity($this->crop1);
    $crop_handler->save();

    $crop_url = drupal_realpath($crop_handler->getUri());
    $reference_size = filesize($this->referenceImage);

    $this->assertEqual(filesize($crop_url), $reference_size, 'The saved crop is identical to the reference image.');

    $orig2 = $this->createImageEntity('cropped_image2.jpg');
    $this->crop2 = CropEntityUtility::buildCropEntity($orig2, '16:9', 10, 10, 800, 450);
    $crop_handler2 = new CropEntity($this->crop2);
    $crop_handler2->save();
    $crop_url2 = drupal_realpath($crop_handler2->getUri());

    $this->assertNotEqual(filesize($crop_url2), $reference_size, 'Crop with different offset is not identical to reference image');
  }

  /**
   * Test that the image file is replaced when viewing a node.
   */
  protected function doTestImageReplacement() {
    CropEntityUtility::saveCropRelation($this->node->nid, 'node', $this->crop1->fid);
    $render_array = node_view($this->node);
    $image_item = $render_array['field_image'][0]['#item'];

    $this->assertEqual($image_item['fid'], $this->originalImage->fid, 'The image on the node is the original non-cropped image.');
    $this->assertEqual($image_item['uri'], $this->crop1->uri, 'The uri for the node image is the same as the image crop.');
  }

  /**
   * Test that the crop link is present on the node form.
   */
  protected function doTestEmptyCropWidget() {
    $this->drupalGet('node/' . $this->node->nid . '/edit');
    $this->assertRaw('href="/crop_entity/file/add/', 'The crop link is visible on the node form');
  }

  /**
   * Test that the crop link is present on the node form.
   */
  protected function doTestPopulatedCropWidget() {
    $this->drupalGet('node/' . $this->node->nid . '/edit');
    $this->assertRaw('id="crop-entity-preview-' . $this->originalImage->fid . '"', 'The crop preview is visible on the node form');
  }

  /**
   * Piggy-backed from file entity module.
   */
  protected function createImageEntity($filepath = 'cropped_image.jpg') {
    $file = new stdClass();

    // Populate defaults array.
    $settings = array(
      'filepath' => $filepath,
      'filemime' => 'image/jpeg',
      'uid' => 1,
      'timestamp' => REQUEST_TIME,
      'status' => FILE_STATUS_PERMANENT,
      'scheme' => file_default_scheme(),
      'type' => 'image',
    );

    $module_path = drupal_get_path('module', 'crop_entity');
    $file_contents = file_get_contents(realpath($module_path . '/test/test_image.jpg'));
    $filepath = $settings['scheme'] . '://' . $settings['filepath'];

    file_put_contents($filepath, $file_contents);
    $this->assertTrue(is_file($filepath), t('The test file exists on the disk.'), 'Create test file');

    $file = new stdClass();
    $file->uri = $filepath;
    $file->filename = drupal_basename($file->uri);
    $file->filemime = $settings['filemime'];
    $file->uid = $settings['uid'];
    $file->timestamp = $settings['timestamp'];
    $file->filesize = filesize($file->uri);
    $file->status = $settings['status'];
    $file->type = $settings['type'];

    // Write the record directly rather than calling file_save() so we don't
    // invoke the hooks.
    $this->assertNotIdentical(drupal_write_record('file_managed', $file), FALSE, t('The file was added to the database.'), 'Create test file');

    return $file;
  }
  /**
   * Creates a new file field.
   *
   * @param string $name
   *   The name of the new field (all lowercase), exclude the "field_" prefix.
   * @param string $type_name
   *   The node type that this field will be added to.
   * @param array $field_settings
   *   A list of field settings that will be added to the defaults.
   * @param array $instance_settings
   *   A list of instance settings that will be added to the instance defaults.
   * @param array $widget_settings
   *   A list of widget settings that will be added to the widget defaults.
   */
  protected function createFileField($name, $type_name, array $field_settings = array(), array $instance_settings = array(), array $widget_settings = array()) {
    $field = array(
      'field_name' => $name,
      'type' => 'file',
      'settings' => array(),
      'cardinality' => !empty($field_settings['cardinality']) ? $field_settings['cardinality'] : 1,
    );
    $field['settings'] = array_merge($field['settings'], $field_settings);
    field_create_field($field);

    $this->attachFileField($name, 'node', $type_name, $instance_settings, $widget_settings);
  }

  /**
   * Attaches a file field to an entity.
   *
   * @param string $name
   *   The name of the new field (all lowercase), exclude the "field_" prefix.
   * @param string $entity_type
   *   The entity type this field will be added to.
   * @param string $bundle
   *   The bundle this field will be added to.
   * @param array $instance_settings
   *   A list of instance settings that will be added to the instance defaults.
   * @param array $widget_settings
   *   A list of widget settings that will be added to the widget defaults.
   */
  protected function attachFileField($name, $entity_type, $bundle, $instance_settings = array(), $widget_settings = array()) {
    $instance = array(
      'field_name' => $name,
      'label' => $name,
      'entity_type' => $entity_type,
      'bundle' => $bundle,
      'required' => !empty($instance_settings['required']),
      'settings' => array(),
      'widget' => array(
        'type' => 'media_generic',
        'settings' => array(),
      ),
    );
    $instance['settings'] = array_merge($instance['settings'], $instance_settings);
    $instance['widget']['settings'] = array_merge($instance['widget']['settings'], $widget_settings);
    field_create_instance($instance);
  }

}
