<?php
/**
 * @file
 * Admin functions for Crop entity.
 */

/**
 * Form callback for Crop entity settings.
 */
function crop_entity_settings($form, &$form_state) {
  $form['crop_entity_dialog_options'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('Crop dialog options'),
  );
  $dialog_options = variable_get('crop_entity_dialog_options', array('width' => 800, 'height' => 650));

  $form['crop_entity_dialog_options']['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Dialog width'),
    '#default_value' => $dialog_options['width'],
  );
  $form['crop_entity_dialog_options']['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Dialog height'),
    '#default_value' => $dialog_options['height'],
  );
  $form['crop_entity_dialog_options']['preview_max_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Dialog preview max width for landscape format'),
    '#default_value' => $dialog_options['preview_max_width'],
  );
  $form['crop_entity_dialog_options']['preview_max_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Dialog preview max height for portrait format'),
    '#default_value' => $dialog_options['preview_max_height'],
  );

  $form['crop_entity_preview_image_style'] = array(
    '#type' => 'textfield',
    '#title' => t('Image style of crop preview'),
    '#default_value' => variable_get('crop_entity_preview_image_style', 'medium'),
  );
  return system_settings_form($form);
}
