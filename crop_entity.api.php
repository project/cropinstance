<?php
/**
 * @file
 * API documentation for Crop Entity.
 */

/**
 * Inform Crop Entity about supported widgets.
 */
function hook_crop_entity_supported_widgets() {
  return array(
    'image_image',
  );
}

/**
 * Allows other modules to alter the list of supported widgets.
 *
 * @param array $widgets
 *   List of supported widgets.
 *
 * @see hook_crop_entity_supported_widgets()
 */
function hook_crop_entity_supported_widgets_alter(array &$widgets) {
  $widgets[] = 'widget_name';
}

/**
 * Register preset ratios for cropping.
 *
 * @return array
 *   Keyed array with info about the new crop ratio(s).
 *   Key - machine-readable identifier
 *    - label: Human readable label for the crop ratio.
 *    - width: Width relative to the ratio height. For 16:9 the width is 16.
 *    - height: Height relative to the width.
 *    - min_width: The smallest allowed crop width.
 *    - min_height: The smallest allowed crop height.
 */
function hook_crop_entity_ratio_info() {
  return array(
    '16:9' => array(
      'label' => t('16:9'),
      'width' => 16,
      'height' => 9,
      'min_width' => 80,
      'min_height' => 45,
    ),
  );
}

/**
 * Alter ratio presets.
 *
 * @param array $ratios
 *   The registered ratios.
 *
 * @see hook_crop_entity_ratio_info()
 */
function hook_crop_entity_ratio_info_alter(array &$ratios) {
  $ratios['16:9']['min_width'] = 160;
}
