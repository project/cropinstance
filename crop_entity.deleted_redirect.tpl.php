<?php
/**
 * @file
 * Template for the page the user is redirected to after deleting crop.
 */
?>
<script>
  var previewId = 'crop-entity-preview-<?php print $fid; ?>';
  var linkId = 'crop-entity-wrapper-<?php print $fid; ?>';
  var editUrl = '/crop_entity/file/add/<?php print $fid; ?>';
  window.parent.document.getElementById(previewId).src = "<?php print $url; ?>";
  window.parent.document.getElementById(previewId).removeAttribute("height");
  window.parent.document.getElementById(linkId).href = editUrl;
  window.parent.CropEntityWidget.dialogDestroy();
</script>
