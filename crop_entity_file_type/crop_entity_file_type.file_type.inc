<?php
/**
 * @file
 * Feature crop_entity_file_type.file_type.inc.
 */

/**
 * Implements hook_file_default_types().
 */
function crop_entity_file_type_file_default_types() {
  $export = array();

  $file_type = new stdClass();
  $file_type->disabled = FALSE; /* Edit this to true to make a default file_type disabled initially */
  $file_type->api_version = 1;
  $file_type->type = 'crop_entity';
  $file_type->label = 'Crop';
  $file_type->description = '';
  $file_type->mimetypes = array();
  $export['crop_entity'] = $file_type;

  return $export;
}
