<?php
/**
 * @file
 * Feature crop_entity_file_type.features.inc.
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function crop_entity_file_type_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "file_entity" && $api == "file_type") {
    return array("version" => "1");
  }
}
