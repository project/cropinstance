<?php
/**
 * @file
 * Forms for editing and creating crop entities.
 */

use Drupal\crop_entity\CropEntity;
use Drupal\crop_entity\CropEntityUtility;

/**
 * Form callback for crop editing form.
 *
 * @param array $form
 *   Standard form array.
 * @param array $form_state
 *   Standard Form alter.
 * @param object $file
 *   A crop entity or an image entity. If the former, an edit form will be
 *   displayed. Otherwise a new crop will be built.
 *
 * @return mixed
 *   A form array.
 */
function crop_entity_crop_entity_edit_form(array $form, array &$form_state, $file) {
  crop_entity_crop_entity_form_build($form, $file);
  return $form;
}


/**
 * Primary form building for the crop form.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 * @param object $file
 *   File entity. Either a crop or an image file.
 *
 * @return mixed
 *   Form array
 */
function crop_entity_crop_entity_from_image_form(array $form, array &$form_state, $file) {
  crop_entity_crop_entity_form_build($form, $file);
  $params = drupal_get_query_parameters();

  if (!empty($params['ref_id']) && !empty($params['ref_type'])) {
    $form['ref_type'] = array(
      '#type' => 'hidden',
      '#value' => check_plain($params['ref_type']),
    );

    $form['ref_id'] = array(
      '#type' => 'hidden',
      '#value' => intval($params['ref_id']),
    );

    $form['parent_key'] = array(
      '#type' => 'hidden',
      '#value' => intval($params['child_key']),
    );
  }

  array_unshift($form['#submit'], 'crop_entity_from_image_submit');
  $form['#submit'][] = 'crop_entity_submit_relate_to_entity';

  return $form;
}

/**
 * Form callback for crop editing form.
 *
 * @param array $form
 *   Standard form array.
 * @param object $file
 *   A crop entity or an image entity. If the former, an edit form will be
 *   displayed. Otherwise a new crop will be built.
 *
 * @return mixed
 *   A form array.
 */
function crop_entity_crop_entity_form_build(array &$form, $file) {
  if (module_exists('admin_menu')) {
    module_invoke('admin_menu', 'suppress');
  }

  $crop = new CropEntity($file);
  $path = drupal_get_path('module', 'crop_entity');
  $ratios = crop_entity_ratio_info();
  $original_image = $crop->getOriginalImage();

  // This is rather blunt, but a much smaller operation than a theme hook.
  $css = '#branding { display: none; } body #page {padding-top: 0; }'
    . ' .crop-entity-source-image:before { content: attr(data-width)"x"attr(data-height);'
    . ' padding: 0 4px; font-size: .8em; background: #dadada; float:right;}';
  drupal_add_css($css, array('type' => 'inline'));

  drupal_add_css($path . '/css/crop_entity.css', array('type' => 'file'));
  drupal_add_js($path . '/js/crop_entity.js', 'file');
  libraries_load('jquery.imgareaselect');
  $form['#attached']['library'] = array('imgareaselect');

  $dialog_options = variable_get('crop_entity_dialog_options', array(
    'preview_max_width' => 730,
    'preview_max_height' => 500,
  ));

  $widget_settings = array(
    'preview_max_width' => $dialog_options['preview_max_width'],
    'preview_max_height' => $dialog_options['preview_max_height'],
  );
  drupal_add_js(array('crop_entity_preview' => $widget_settings), 'setting');

  _crop_entity_form_settings_js($crop, $ratios);

  // Similarly structured fields.
  $numeric_fields = array(
    'x_offset' => 'getX',
    'y_offset' => 'getY',
    'crop_width' => 'getWidth',
    'crop_height' => 'getHeight',
    'fid' => 'getId',
  );

  foreach ($numeric_fields as $field_name => $getter) {
    $form[$field_name] = array(
      '#type' => 'hidden',
      '#default_value' => $crop->$getter(),
      '#attributes' => array('data-field_name' => $field_name),
    );
  }

  // Get ratios and build a selector.
  array_walk($ratios, function(&$item, $key) {
    $item = $item['label'];
  });

  $form['croptop'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#attributes' => array('class' => array('crop-entity-top-wrap')),
  );
  $form['croptop']['submit_top'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#attributes' => array('class' => array('crop-entity-top-submit')),
  );

  $form['croptop']['crop_ratio'] = array(
    '#type' => 'select',
    '#title' => t('Select ratio'),
    '#options' => $ratios,
    '#default_value' => $crop->getRatio(),
    '#attributes' => array('class' => array('crop-entity-ratio-select')),
  );

  $link_options = array(
    'absolute' => TRUE,
    'attributes' => array('class' => 'crop-entity-ratio-maximize button'),
    'fragment' => 'max',
  );
  $max_link = l(t('Maximize selection'), '', $link_options);
  $form['croptop']['maximize'] = array(
    '#markup' => $max_link,
  );
  $link_options = array(
    'absolute' => TRUE,
    'attributes' => array('class' => 'crop-entity-clear-selection button'),
    'fragment' => 'clear',
  );
  $clear_link = l(t('Clear selection'), '', $link_options);
  $form['croptop']['clear'] = array('#markup' => $clear_link);

  // The original image to crop. We need the original size.
  $original = '<img src="' . file_create_url($original_image->uri) . '" />';
  $form['crop_area'] = array(
    '#prefix' => '<div data-width="" data-height="" class="crop-entity-source-image">',
    '#suffix' => '</div>',
    '#markup' => $original,
  );
  $form['original_image_fid'] = array(
    '#type' => 'hidden',
    '#value' => $original_image->fid,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['#submit'][] = 'crop_entity_crop_entity_form_submit';

  return $form;
}

/**
 * Submit handler when the crop was created from an image.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 */
function crop_entity_from_image_submit(array $form, array &$form_state) {
  $original_image = file_load($form_state['values']['original_image_fid']);
  $crop_entity = CropEntityUtility::buildEmptyCrop($original_image);
  $form_state['crop_entity'] = $crop_entity;
}

/**
 * Submit handler for the crop editing form.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Form state array.
 */
function crop_entity_crop_entity_form_submit(array $form, array &$form_state) {
  $fid = intval($form_state['values']['fid']);
  if (empty($fid) && !empty($form_state['crop_entity'])) {
    $crop = new CropEntity($form_state['crop_entity']);
  }
  else {
    $crop = new CropEntity($fid);
  }

  if (empty($crop)) {
    drupal_set_message(t('The crop could not be saved'), 'error');
    return;
  }

  // Save the simple values to the crop object.
  $crop_values = array(
    'x_offset',
    'y_offset',
    'crop_width',
    'crop_height',
  );

  $values_to_save = array(
    'field_crop_ratio' => $form_state['values']['croptop']['crop_ratio'],
  );
  foreach ($crop_values as $key) {
    if (isset($form_state['values'][$key])) {
      $values_to_save['field_' . $key] = $form_state['values'][$key];
    }
  }

  if (!empty($values_to_save)) {
    $crop->setValues($values_to_save);
  }

  try {
    $crop->save();
  }
  catch (\Exception $e) {
    drupal_set_message(t('The crop could not be saved'), 'error');
    drupal_set_message($e->getMessage(), 'error');
    return;
  }

  // Save the crop object for added submit hooks.
  $form_state['crop_entity_crop'] = $crop;
  $cfid = intval($crop->getId());

  // If the crop file ID is empty, the crop is deleted, so the original is used
  // instead.
  if (empty($cfid)) {
    $original = $crop->getOriginalImage();
    $cfid = $original->fid;
  }

  $form_state['redirect'] = 'crop_entity/saved/' . $cfid;
}

/**
 * Submit handler to create an entity relation.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Form state array.
 */
function crop_entity_submit_relate_to_entity(array $form, array &$form_state) {
  $cfid = $form_state['crop_entity_crop']->getId();

  if (
    !empty($form_state['values']['ref_id']) &&
    !empty($form_state['values']['ref_type']) &&
    !empty($form_state['values']['crop_width']) &&
    !empty($form_state['values']['crop_height'])
  ) {
    CropEntityUtility::saveCropRelation($form_state['values']['ref_id'], $form_state['values']['ref_type'], $cfid);
  }
}

/**
 * Add required JS settings for the crop form.
 *
 * @param CropEntity $crop
 *   The current crop entity wrapper.
 * @param array $ratios
 *   The registered ratios.
 */
function _crop_entity_form_settings_js(CropEntity $crop, array $ratios) {
  $ratio_keys = array_keys($ratios);
  $default_ratio_key = $crop->getRatio() ? $crop->getRatio() : array_shift($ratio_keys);
  $default_ratio = $ratios[$default_ratio_key];
  $aspectratio = $default_ratio['width'] . ':' . $default_ratio['height'];

  // Set defaults for use with the crop tool.
  $defaults = array(
    'x1' => intval($crop->getX()),
    'y1' => intval($crop->getY()),
    'x2' => intval($crop->getX() + $crop->getWidth()),
    'y2' => intval($crop->getY() + $crop->getHeight()),
    'aspectRatio' => $aspectratio,
  );

  $settings = array(
    'crop_entity_defaults' => $defaults,
    'crop_entity_ratios' => $ratios,
  );

  drupal_add_js($settings, array('type' => 'setting'));
}

/**
 * Page callback to update parent window preview.
 *
 * @param object $file
 *   The recently saved crop entity.
 *
 * @return string
 *   Themed output of the redirect page.
 */
function crop_entity_saved_redirect($file) {
  if (module_exists('admin_menu')) {
    module_invoke('admin_menu', 'suppress');
  }

  if ($file->type == 'image') {
    $vars = array(
      'fid' => $file->fid,
      'url' => image_style_url('medium', $file->uri),
    );

    $output = theme('crop_entity_deleted_redirect', $vars);

    return $output;
  }

  $vars = array(
    'fid' => $file->field_crop_original_image[LANGUAGE_NONE][0]['fid'],
    'cfid' => $file->fid,
    'url' => image_style_url('medium', $file->uri),
  );

  $output = theme('crop_entity_save_redirect', $vars);

  return $output;
}
