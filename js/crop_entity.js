/**
 * @file
 * Browser functionality for the crop dialog form.
 */

/* global Drupal:true */

"use strict";

var CropEntity = {scale: 1};
(function ($) {
  // Set form values (from the imgareaselect plugin).
  CropEntity.setValues = function(values) {
    for (var field in values) {
      $('input[name="' + field + '"]').val(values[field]);
    }
  };
  // Get a ratio for use with the imgareaselect plugin.
  CropEntity.getRatio = function(ratio) {
    if ((ratio.height < 1) || (ratio.width < 1)) {
      return false;
    }
    var rendered_ratio = ratio.width + ':' + ratio.height;
    return rendered_ratio;
  };
  // The image for cropping is resized in the dialog. Calculate
  // size of the crop.
  CropEntity.calculateResize = function(scale, value) {
    return Math.round(value / scale);
  };

  /**
   * Calculate new selection when changing ratio.
   *
   * Recalcute from width first. If height exceeds boundaries of the image,
   * recalculate from height.
   *
   * @param {object} selection
   *   A imgAreaSelect selection object.
   * @param {object} ratio
   *   A Crop entity ratio entry from settings.
   * @param {object} image
   *   jQuery object with the image object.
   *
   * @return {object}
   *   A selection object with new values set.
   */
  CropEntity.changeRatio = function(selection, ratio, image) {
    if ((ratio.width == 0) || (ratio.height == 0)) {
      return selection;
    }
    var imgW = image.width(),
      imgH = image.height(),
      scale = ratio.height / ratio.width;

    var newHeight = Math.round(selection.width * scale);

    if (newHeight < imgH) {
      if ((newHeight + selection.y1) > imgH) {
        selection.y1 -= ((newHeight + selection.y1) - imgH);
      }
      selection.y2 = (selection.y1 + newHeight);
      selection.height = newHeight;
      return selection;
    }

    // Since the crop would be too high for the original image, we assume that
    // the width will be ok.
    scale = ratio.width / ratio.height;
    var newWidth = Math.round(selection.height * scale);
    if ((newWidth + selection.x1) > imgW) {
      selection.x1 -= ((newWidth + selection.x1) - imgW);
    }
    selection.x2 = selection.x1 + newWidth;
    selection.width = newWidth;
    return selection;
  };

  /**
   * Maximize selection of selected crop ratio.
   *
   * @param {object} selection
   *   ImgAreaSelect selection object.
   * @param {object} ratio
   *   Ratio entry from Drupal.settings.
   * @param {object} image
   *   jQuery object with the image preview.
   */
  CropEntity.maxSelection = function(selection, ratio, image) {

    // If the ratio is wider than or equal to the image´s, use the image width
    // to define size (cause then it will not be too high). Otherwise, use the
    // image height.
    var imgW = image.width();
    var imgH = image.height();

    // If any of the ratio parameters are 0, select the whole image.
    if ((ratio.width == 0) || (ratio.height == 0)) {
      selection.x1 = 0;
      selection.y1 = 0;
      selection.x2 = imgW;
      selection.y2 = imgH;
      selection.width = imgW;
      selection.height = imgH;

      return selection;
    }

    var ratioRatio = ratio.width / ratio.height;
    var imgRatio = imgW / imgH;

    if (ratioRatio >= imgRatio) {
      selection.width = imgW;
      selection.height = selection.width / ratioRatio;
      selection.x1 = 0;
      selection.x2 = selection.width;

      if ((selection.height + selection.y1) > imgH) {
        selection.y1 -= ((selection.height + selection.y1) - imgH);
      }
      selection.y2 = selection.y1 + selection.height;
      return selection;
    }

    // The image is wider than the ratio.
    selection.height = imgH;
    selection.width = selection.height * ratioRatio;
    selection.y1 = 0;
    selection.y2 = selection.height;

    if ((selection.width + selection.x1) > imgW) {
      selection.x1 -= ((selection.width + selection.x1) - imgW);
    }
    selection.x2 = selection.x1 + selection.width;

    return selection;
  };

  CropEntity.convertAndSetValues = function(selection) {
    var values = {
      'x_offset': this.calculateResize(this.scale, selection.x1),
      'y_offset': this.calculateResize(this.scale, selection.y1),
      'crop_width': this.calculateResize(this.scale, selection.width),
      'crop_height': this.calculateResize(this.scale, selection.height)
    };

    this.setValues(values);
  };

  CropEntity.updateDataAttributes = function(selection, wrapper) {

    wrapper.attr('data-width', this.calculateResize(this.scale, selection.width));
    wrapper.attr('data-height', this.calculateResize(this.scale, selection.height));
  }

  Drupal.behaviors.crop_entity = {
    attach: function () {

      // Load defaults.
      var crop_defaults = Drupal.settings.crop_entity_defaults;
      var ratios = Drupal.settings.crop_entity_ratios;
      var originalImage = $('.crop-entity-source-image img');
      var originalImageWrapper = $('.crop-entity-source-image');
      var imageArea, origWidth, origHeight, scale;
      var previewLimits = Drupal.settings.crop_entity_preview;

      var maximizeSelection = function(imgArea, origImage, ratio) {
        var oldSelection = imgArea.getSelection();
        var newSelection = CropEntity.maxSelection(oldSelection, ratio, origImage);
        imgArea.setSelection(newSelection.x1, newSelection.y1, newSelection.x2, newSelection.y2);
        imgArea.update();
        CropEntity.convertAndSetValues(newSelection);
      }

      // Original image needs to be loaded before we proceed.
      originalImage.one('load', function() {

        // Get the size of the original image, resize it to fit the window,
        // calculate the ratio and apply it to all measures.
        origWidth = originalImage.width();
        origHeight = originalImage.height();
        var originalImageCSS;

        if ((origWidth < origHeight) && previewLimits) {
          originalImageCSS = {width: 'auto', height: '100%', "max-height": previewLimits.preview_max_height + 'px'};
        }
        else if (previewLimits) {
          originalImageCSS = {height: 'auto', width: '100%', "max-width": previewLimits.preview_max_width + 'px'};
        }
        else {
          originalImageCSS = {height: 'auto', width: '100%'};
        }

        originalImage.css(originalImageCSS);

        CropEntity.scale = originalImage.width() / origWidth;

        $.each(['x1', 'y1', 'x2', 'y2'], function(index, setting) {
          crop_defaults[setting] *= CropEntity.scale;
        });

        // Misc. settings for imageAreaSelect. Recalculate dimensions
        // after selection.
        crop_defaults.handles = true;
        crop_defaults.instance = true;
        crop_defaults.onInit = function(img, selection) {
          CropEntity.updateDataAttributes(selection, originalImageWrapper);
        }

        crop_defaults.onSelectEnd = function(img, selection) {
          CropEntity.convertAndSetValues(selection);
          CropEntity.updateDataAttributes(selection, originalImageWrapper);
        };

        imageArea = originalImage.imgAreaSelect(crop_defaults);

        // If defaults are empty, build a selection.
        if ((crop_defaults.x2 == 0) || (crop_defaults.x2 == 0)) {
          maximizeSelection(imageArea, originalImage, ratios[$('select.crop-entity-ratio-select').val()]);
        }
      }).each(function() {
        if (this.complete) {
          $(this).trigger('load');
        }
      });

      // Ratio selector menu.
      $('select.crop-entity-ratio-select').on('change', function(e) {
        var oldSelection = imageArea.getSelection();
        var newSelection = CropEntity.changeRatio(oldSelection, ratios[$(this).val()], originalImage);

        // Calculate new width and height from width and check if it
        // exceeds thew image size. If it does, calculate from height.
        var ratio = CropEntity.getRatio(ratios[$(this).val()]);

        imageArea.setOptions({aspectRatio: ratio});
        imageArea.setSelection(newSelection.x1, newSelection.y1, newSelection.x2, newSelection.y2);
        imageArea.update();
        CropEntity.convertAndSetValues(newSelection);
      });

      $('.crop-entity-ratio-maximize').on('click', function(ev){
        ev.preventDefault();
        var ratio = ratios[$('select.crop-entity-ratio-select').val()];
        maximizeSelection(imageArea, originalImage, ratio);
        CropEntity.convertAndSetValues(selection);
      });

      $('.crop-entity-clear-selection').on('click', function(ev){
        ev.preventDefault();
        imageArea.cancelSelection();
        var emptySelection = imageArea.getSelection();
        CropEntity.convertAndSetValues(emptySelection);
      });

    }
  };
})(jQuery);
