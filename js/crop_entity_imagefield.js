/**
 * @file
 * For use with the image widget (e.g. on node form).
 */

/* global Drupal:true, jQuery:true */

"use strict";

var CropEntityWidget = {
  dialogOptions: {
    width: 800,
    height: 650,
    modal: true,
    resizable: true,
    autoOpen: true,
    close: function (event, ui) {
      CropEntityWidget.dialogDestroy();
    }
  },
  dialogDestroy: function () {
    var dialog = jQuery('.crop-entity-dialog');
    dialog.dialog('destroy');
    dialog.remove();
  }
};

(function ($) {
  $.fn.cropEntityIframe = function(previewWidth, previewHeight) {
    var href = this.attr('href');
    var fillDialog = {width: '100%', height: '100%', 'min-height': previewHeight + 'px', 'min-width': previewWidth + 'px'};
    var dialog = $('<div class="crop-entity-dialog"></div>');
    var iframe = $('<iframe src="' + href + '" frameborder="0" />');
    iframe.css(fillDialog);
    iframe.appendTo(dialog);
    dialog.appendTo('body').dialog(CropEntityWidget.dialogOptions);
    dialog.css(fillDialog);
  };

  Drupal.behaviors.crop_entity_imagefield = {
    attach: function () {
      if (Drupal.settings.crop_entity) {
        CropEntityWidget.dialogOptions.width = Drupal.settings.crop_entity.dialog_width;
        CropEntityWidget.dialogOptions.height = Drupal.settings.crop_entity.dialog_height;
      }

      var previewWidth = CropEntityWidget.dialogOptions.width - 10;
      var previewHeight = CropEntityWidget.dialogOptions.height - 110;

      $('.crop-entity-form').unbind('click').on('click', function(event) {
        $(this).cropEntityIframe(previewWidth, previewHeight);
        event.preventDefault();
        event.stopPropagation();
      });
    }
  };
})(jQuery);
