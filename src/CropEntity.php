<?php
/**
 * @file
 * Wrapper for the crop entity.
 */

namespace Drupal\crop_entity;

/**
 * Class CropEntity.
 *
 * A wrapper around the manualcrop entity for easier access of the properties.
 * Only relevant for creating and changing the entity.
 */
class CropEntity {
  protected $cropEntity;
  protected $sourceImage;

  /**
   * Load crop entity if an ID is supplied.
   *
   * @param object|int $cfid
   *   File ID of a crop entity or a file or a fully loaded of either.
   */
  public function __construct($cfid = 0) {
    if (empty($cfid)) {
      return;
    }

    if (is_int($cfid)) {
      $cfid = file_load($cfid);
    }

    if (is_object($cfid) && ($cfid->type == 'image')) {
      $this->cropEntity = CropEntityUtility::buildCropEntity($cfid);
      $this->sourceImage = $cfid;
    }
    elseif (is_object($cfid) && ($cfid->type == 'crop_entity')) {
      $this->cropEntity = $cfid;
      $this->getOriginalImage();
    }
  }

  /**
   * Create a cropped image from a source.
   *
   * @param object $image
   *   A Drupal image object.
   *
   * @return bool
   *   Whether or not the cropping succeeded.
   */
  protected function cropSourceImage(&$image) {
    module_load_include('inc', 'image', 'image.effects');
    $data['width'] = intval($this->getWidth());
    $data['height'] = intval($this->getHeight());
    $data['anchor'] = intval($this->getX()) . '-' . intval($this->getY());

    if (!image_crop_effect($image, $data)) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Create a derivative and save the image.
   *
   * @param null|string $source
   *   URI of the source image.
   * @param null|string $destination
   *   URI of the destination image.
   *
   * @return bool
   *   Was the creation successful.
   */
  protected function createDerivative($source = NULL, $destination = NULL) {
    if (empty($source)) {
      $source = $this->sourceImage->uri;
    }

    if (empty($destination)) {
      $destination = $this->createUri();
    }
    // If the source file doesn't exist, return FALSE without creating folders.
    if (!$image = image_load($source)) {
      return FALSE;
    }

    // The storage needs to be cleared before we can create a new image and we
    // do some cleanup.
    if (module_exists('storage_core_bridge')) {
      $uris_to_reset = array($destination);
      if ($this->cropEntity->uri !== $source) {
        $uris_to_reset[] = $this->cropEntity->uri;
      }

      db_delete('storage_core_bridge')
        ->condition('uri', $uris_to_reset, 'IN')
        ->execute();
    }

    // Get the folder for the final location of this style.
    $directory = drupal_dirname($destination);

    // Build the destination folder tree if it doesn't already exist.
    if (!file_prepare_directory($directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
      watchdog('image', 'Failed to create style directory: %directory', array('%directory' => $directory), WATCHDOG_ERROR);
      return FALSE;
    }

    // Create the cropped image.
    $this->cropSourceImage($image);

    if (!image_save($image, $destination)) {
      if (file_exists($destination)) {
        watchdog('image', 'Cached image file %destination already exists. Your rewrite configuration may need to be changed.', array('%destination' => $destination), WATCHDOG_ERROR);
      }
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Update the usage table with a new uri.
   *
   * @param object $crop
   *   Crop entity.
   *
   * @return bool
   *   If the table was updated.
   *
   * @throws \PDOException
   *   If the DB merge failed.
   */
  protected function saveCropUriInUsageTable($crop) {
    if (empty($crop->fid)) {
      return FALSE;
    }

    $result = db_select('crop_entity_usage', 'ceu')
      ->fields('ceu')
      ->condition('cfid', $crop->fid)
      ->execute()
      ->fetchObject();

    if (empty($result) || $result->crop_uri == $crop->uri) {
      return FALSE;
    }
    try {
      db_merge('crop_entity_usage')
        ->key(array('cfid' => $crop->fid))
        ->fields(array('crop_uri' => $crop->uri))
        ->execute();
    }
    catch (\PDOException $error) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Create a standardized URI for the image.
   *
   * @param string|null $scheme
   *   Optional scheme for the file.
   *
   * @return string
   *   The URI.
   */
  protected function createUri($scheme = NULL) {
    if (empty($scheme)) {
      $scheme = file_default_scheme();
    }
    $time = time();
    $date = date('Y/m/d/', $time);
    $filename = drupal_basename($this->sourceImage->uri);
    $uri = $scheme . '://images/crop/' . $date . $time . '_' . $filename;

    return $uri;
  }

  /**
   * Set the parent/source image for the crop.
   *
   * @param int $fid
   *   File Id of the source image.
   */
  public function setOriginalImage($fid) {
    $this->cropEntity->field_crop_original_image[LANGUAGE_NONE][0] = array(
      'fid' => $fid,
    );
  }

  /**
   * Update the cropped file with the available information.
   *
   * @param string $uri
   *   Image URI for the a new image.
   * @param string $filename
   *   Filename for the entity.
   *
   * @return bool
   *   Whether or not the derivative was created.
   */
  public function updateImageFile($uri, $filename) {
    if ($this->createDerivative(NULL, $uri)) {
      $this->cropEntity->uri = $uri;
      $this->cropEntity->filename = $filename;
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get the crop entity fid.
   *
   * @return int
   *   The fid of the crop.
   */
  public function getId() {
    if (empty($this->cropEntity->fid)) {
      return 0;
    }
    return $this->cropEntity->fid;
  }

  /**
   * Get the width of the crop.
   *
   * @return int
   *   The width in pixels.
   */
  public function getWidth() {
    return $this->getFieldValue($this->cropEntity->field_crop_width);
  }

  /**
   * Get the height of the crop.
   *
   * @return int
   *   The height in pixels.
   */
  public function getHeight() {
    return $this->getFieldValue($this->cropEntity->field_crop_height);
  }

  /**
   * Get the horizontal offset of the crop.
   *
   * @return int
   *   The offset in pixels.
   */
  public function getX() {
    return $this->getFieldValue($this->cropEntity->field_x_offset);
  }

  /**
   * Get the vertical offset of the crop.
   *
   * @return int
   *   The offset in pixels.
   */
  public function getY() {
    return $this->getFieldValue($this->cropEntity->field_y_offset);
  }

  /**
   * Get the uri of the crop.
   *
   * @return string
   *   The Drupal URI for the cropped image.
   */
  public function getUri() {
    return $this->cropEntity->uri;
  }

  /**
   * Get the ratio of the crop.
   *
   * @param bool $full
   *   Set to true to get the full ratio array.
   *
   * @return array|string
   *   The full ratio registration or the ratio key.
   */
  public function getRatio($full = FALSE) {
    $ratio_key = $this->getFieldValue($this->cropEntity->field_crop_ratio);

    if (!$full) {
      return $ratio_key;
    }

    $ratio = crop_entity_ratio_info($ratio_key);
    return $ratio;
  }

  /**
   * Get the file object of the original image.
   *
   * @return mixed
   *   A file entity object.
   */
  public function getOriginalImage() {
    if (!empty($this->sourceImage)) {
      return $this->sourceImage;
    }

    $file_data = $this->cropEntity->field_crop_original_image[LANGUAGE_NONE][0];
    if (empty($file_data)) {
      return NULL;
    }
    $this->sourceImage = file_load($file_data['fid']);
    if (empty($file_data['width'])) {
      $image_size = image_get_info($this->sourceImage->uri);
    }
    else {
      $image_size = $file_data;
    }
    $this->sourceImage->width = $image_size['width'];
    $this->sourceImage->height = $image_size['height'];
    return $this->sourceImage;

  }

  /**
   * Update crop values.
   *
   * @param array $values
   *   Keyed array with values to update the crop with.
   */
  public function setValues(array $values) {
    array_walk($values, function($value, $field_name, &$crop) {
      CropEntityUtility::setFieldValue($crop, $field_name, $value);

    }, $this->cropEntity);
  }

  /**
   * Persist the wrapped entity to the database.
   */
  public function save() {
    // Crops cannot have a width nor a height of 0.
    if (($this->getWidth() == 0) || ($this->getHeight() == 0)) {
      $fid = $this->getId();
      if ($fid) {
        // Ensure that the original is available. Then delete the crop.
        $this->getOriginalImage();
        file_delete($this->cropEntity, TRUE);
        $this->cropEntity = new \stdClass();
        CropEntityUtility::deleteCropRelation($fid);
      }

      return TRUE;
    }

    $uri = $this->createUri();
    $filename = $this->getRatio() . '_' . $this->sourceImage->filename;
    if ($this->updateImageFile($uri, $filename)) {
      try {
        file_save($this->cropEntity);
        $this->saveCropUriInUsageTable($this->cropEntity);
      }
      catch (\PDOException $error) {
        drupal_set_message(t('The file could not be saved. Error message: @error'), array('@error' => check_plain($error)));
      }
    }
    else {
      drupal_set_message(t('The crop was not generated. This can be a write permission problem, a timeout or any other reason. Ask your server administrator if the problem persists.'));
    }
  }

  /**
   * Helper function to retrieve values of deep Drupal arrays.
   *
   * @param array $field
   *   The field array from the entity.
   * @param string $key
   *   The key in the field data array.
   * @param int $delta
   *   The delta for multivalue fields.
   * @param string $language
   *   Language code.
   *
   * @return mixed
   *   Field value or NULL if the field is not set.
   */
  protected static function getFieldValue(array $field, $key = 'value', $delta = 0, $language = LANGUAGE_NONE) {
    if (!isset($field[$language][$delta][$key])) {
      return NULL;
    }
    return $field[$language][$delta][$key];
  }

}
