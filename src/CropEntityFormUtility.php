<?php
/**
 * @file
 * Wrapper for functions used in form processing.
 */

namespace Drupal\crop_entity;

/**
 * Class CropEntityFormUtility.
 *
 * Utility functions for use in forms.
 *
 * @package Drupal\crop_entity
 */
class CropEntityFormUtility {
  /**
   * Tell if the referenced item is an image.
   *
   * Other media items may occur in a supported widget, so the mime type
   * is checked to see if it is an image.
   *
   * @param array $child
   *   Element child from an image widget.
   * @param array $files
   *   An array of the files present in the current widget.
   *
   * @return bool
   *   True if the child is an image (as opposed to a video or similar).
   */
  public static function checkIfImage(array $child, array $files) {
    $fid = $child['fid'];
    $filemime = '';
    if (empty($child['filemime']) && !empty($files[$fid])) {
      $filemime = $files[$fid]->filemime;
    }
    elseif (!empty($child['filemime'])) {
      $filemime = $child['filemime'];
    }

    if (strpos($filemime, 'image') === FALSE) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Get information about the entity a form element resides on.
   *
   * @param array $element
   *   The form element. Should be a media widget.
   *
   * @return array|bool
   *   False if the info cannot be retrieved. Otherwise a keyed array with the
   *   required values.
   */
  public static function getFormEntityInfo(array $element) {
    if (empty($element['#entity'])) {
      $entity = $element[0]['#entity'];
    }
    else {
      $entity = $element['#entity'];
    }

    if (empty($element['#entity_type'])) {
      $entity_type = _crop_entity_get_entity_type($entity);
    }
    else {
      $entity_type = $element['#entity_type'];
    }

    if (!empty($entity_type) && !empty($entity)) {
      $ids = entity_extract_ids($entity_type, $entity);
    }
    else {
      return FALSE;
    }

    return array(
      'entity' => $entity,
      'type' => $entity_type,
      'id' => $ids[0],
    );
  }

  /**
   * Modify a media widget to support crop entity.
   *
   * Add a preview with a link to the crop edit form.
   *
   * @param array $element
   *   Form element. Supported widget.
   */
  public static function processImageWidget(array &$element, $entity_type = '', $translation_original = 0) {
    // Load crops and sort them per file.
    $entity_info = CropEntityFormUtility::getFormEntityInfo($element);

    $crops_by_file = array();
    $e_id = 0;
    if (!empty($entity_info['id'])) {
      $e_id = $entity_info['id'];
    }
    elseif (!empty($translation_original)) {
      $e_id = $translation_original;
    }

    if (empty($entity_type) && !empty($entity_info['type'])) {
      $entity_type = $entity_info['type'];
    }
    if (!empty($e_id)) {
      $crops_by_file = CropEntityFormUtility::getCropPreviewByOriginal($e_id, $entity_type);
    }

    // When returning from the media browser, we only have limited information
    // about each element, so the full files are loaded.
    $files = array();
    $children = element_children($element);
    if (empty($element[$children[0]]['#default_value']['filemime'])) {
      $fids = array();
      foreach (element_children($element) as $key) {
        $fids[] = $element[$key]['#default_value']['fid'];
      }
      $files = file_load_multiple($fids);
    }

    // Process each item in the widget.
    foreach ($children as $key) {
      $child = &$element[$key]['#default_value'];
      $fid = $child['fid'];

      if (!CropEntityFormUtility::checkIfImage($child, $files)) {
        continue;
      }

      if (!empty($translation_original)) {
        $element[$key]['crop_entity_new'] = array(
          '#type' => 'hidden',
          '#attributes' => array(
            'id' => 'crop-entity-parent-' . $fid,
          ),
          '#value' => $crops_by_file[$fid]['#attributes']['data-crop-fid'][0],
        );
      }

      if (!empty($crops_by_file[$fid])) {
        $renderable = $crops_by_file[$fid];
      }
      else {
        // There is no existing crop.
        $fid = $element[$key]['#default_value']['fid'];
        $original_file = file_load($fid);

        $element[$key]['crop_entity_new'] = array(
          '#type' => 'hidden',
          '#attributes' => array(
            'id' => 'crop-entity-parent-' . $fid,
          ),
        );

        $renderable = CropEntityFormUtility::cropEntityPreview($original_file->uri, $fid, $fid, NULL, NULL, $entity_type, $e_id);
      }
      $element[$key]['crop_entity_crop_preview'] = $renderable;
      unset($element[$key]['preview']);
    }
  }

  /**
   * Modify a single element widget to support crop entity.
   *
   * Add a preview with a link to the crop edit form. This method is not in use
   * in the module at the time being, but can be used with custom single element
   * widgets.
   *
   * @param array $element
   *   Form element. Supported widget.
   */
  public static function processSingleElementWidget(array &$element) {
    // Load crops and sort them per file.
    $entity_info = CropEntityFormUtility::getFormEntityInfo($element);
    $fid = $element['fid']['#default_value'];
    $original_file = file_load($fid);
    // Process each item in the widget.
    $child = array('fid' => $fid);

    if (!CropEntityFormUtility::checkIfImage($child, array($fid => $original_file))) {
      return;
    }
    $crops_by_file = array();
    if (!empty($entity_info['id'])) {
      $crops_by_file = CropEntityFormUtility::getCropPreviewByOriginal($entity_info['id'], $entity_info['type']);
    }

    if (!empty($crops_by_file[$fid])) {
      $renderable = $crops_by_file[$fid];
    }
    else {
      $element['crop_entity_new'] = array(
        '#type' => 'hidden',
        '#attributes' => array(
          'id' => 'crop-entity-parent-' . $fid,
        ),
      );

      $renderable = CropEntityFormUtility::cropEntityPreview($original_file->uri, $fid, $fid, NULL, NULL, $entity_info['type'], $entity_info['id']);
    }
    $element['crop_entity_crop_preview'] = $renderable;
    unset($element['preview']);
  }

  /**
   * Get an array of crop previews keyed by original file fid.
   *
   * @param int $entity_id
   *   Referencing entity ID.
   * @param string $entity_type
   *   Referencing entity type.
   *
   * @return array
   *   Renderable array for each preview.
   */
  protected static function getCropPreviewByOriginal($entity_id, $entity_type) {
    $crops_by_file = array();
    $crops = CropEntityUtility::loadCropRelations($entity_id, $entity_type, TRUE);
    foreach ($crops as $cfid => $crop) {
      $original_fid = $crop->field_crop_original_image[LANGUAGE_NONE][0]['fid'];
      $crops_by_file[$original_fid] = CropEntityFormUtility::cropEntityPreview($crop->uri, $cfid, $original_fid, $crop->field_crop_width[LANGUAGE_NONE][0]['value'], $crop->field_crop_height[LANGUAGE_NONE][0]['value']);
      $crops_by_file[$original_fid]['#attributes']['data-crop-fid'] = array($cfid);
    }
    return $crops_by_file;
  }

  /**
   * Get a crop preview as a renderable array.
   *
   * @param string $uri
   *   Image resource URI.
   * @param int $link_fid
   *   File ID for the link. Can be for the crop or original image.
   * @param int $original_fid
   *   File ID of the original image. Will be identical to $link_fid if not
   *   opening a crop.
   * @param int $width
   *   Image width (image resource, not of the preview).
   * @param int $height
   *   Image height (image resource, not of the preview).
   * @param null|string $ref_type
   *   Referencing entity type. Not required for edit links.
   * @param null|int $ref_id
   *   Referencing entity id. Not required for edit links.
   * @param null|int $element_key
   *   Key of the widget child element. Used to update hidden field when saving
   *   new crops. Not required for edit links.
   *
   * @return array
   *   Renderable array with a linked image.
   */
  protected static function cropEntityPreview($uri, $link_fid, $original_fid, $width, $height, $ref_type = NULL, $ref_id = NULL, $element_key = NULL) {
    $variables = array(
      'style_name' => 'medium',
      'path' => $uri,
      'width' => $width,
      'height' => $height,
      'attributes' => array(
        'data-url' => file_create_url($uri),
        'id' => array('crop-entity-preview-' . $original_fid),
      ),
    );
    $markup = theme('image_style', $variables);

    $link_options = array('html' => TRUE);

    // Set query param.
    if (!empty($ref_type) && !empty($ref_id)) {
      $link_options['query'] = array(
        'ref_type' => $ref_type,
        'ref_id' => $ref_id,
        'child_key' => $element_key,
      );
    }

    if ($link_fid == $original_fid) {
      $href = 'crop_entity/file/add/' . $original_fid;
      $class = 'crop-entity-new';
      $desc = t('Click to crop the image');
    }
    else {
      $href = 'crop_entity/file/' . $link_fid;
      $class = 'crop-entity-edit';
      $desc = t('Click to change the image crop');
    }
    $renderable = array(
      '#type' => 'link',
      '#href' => $href,
      '#title' => $markup,
      '#attributes' => array(
        'class' => array('crop-entity-form', $class),
        'id' => array('crop-entity-wrapper-' . $original_fid),
        'title' => $desc,
      ),
      '#options' => $link_options,
    );

    return $renderable;
  }

}
