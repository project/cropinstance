<?php
/**
 * @file
 * Utilities for handling Manual crop entities.
 */

namespace Drupal\crop_entity;

/**
 * Class CropEntityUtility.
 *
 * @package Drupal\ManualCrop
 *
 * Mainly static methods to handle data and loading of manual crop entities.
 */
class CropEntityUtility {
  /**
   * Load crops.
   *
   * @param int|array $referencing_entity_id
   *   ID of referencing entity or an array of IDs.
   * @param string $referencing_entity_type
   *   Type of referencing entity.
   * @param bool $load_full_crop
   *   Whether or not to load a full crop entity.
   * @param string $key
   *   The column to key the results by.
   * @param bool $reset
   *   Set to TRUE to force loading of crops even if a static variable exists.
   *
   * @return array
   *   Crop file IDs or fully loaded crops if $load_full_crop is true.
   */
  public static function loadCropRelations($referencing_entity_id, $referencing_entity_type, $load_full_crop = TRUE, $key = 'cfid', $reset = FALSE) {
    // Build up a key using the referencing entity id(s).
    if (is_array($referencing_entity_id)) {
      // Handle calling this function with an empty array. Otherwise, the
      // resulting query will fail fatally.
      if (empty($referencing_entity_id)) {
        return array();
      }
      $referencing_entity_id_clone = $referencing_entity_id;
      // Make sure the ids are always in the same order.
      sort($referencing_entity_id_clone);
      $referencing_entity_id_key = implode('_', $referencing_entity_id_clone);
    }
    else {
      // Plain key, just use it.
      $referencing_entity_id_key = $referencing_entity_id;
    }
    $result = &drupal_static(__FUNCTION__, array());

    if (!isset($result[$key][$referencing_entity_type][$referencing_entity_id_key])) {
      if (!is_array($referencing_entity_id)) {
        $referencing_entity_id = array($referencing_entity_id);
      }

      $result[$key][$referencing_entity_type][$referencing_entity_id_key] = db_select('crop_entity_usage', 'ceu')
        ->fields('ceu')
        ->condition('referencing_entity_type', $referencing_entity_type)
        ->condition('referencing_entity_id', $referencing_entity_id, 'IN')
        ->orderBy('delta')
        ->execute()
        ->fetchAllAssoc($key);
    }

    if (!$load_full_crop) {
      return return $result[$key][$referencing_entity_type][$referencing_entity_id_key];
    }
    else {
      $crops = file_load_multiple(array_keys($result[$key][$referencing_entity_type][$referencing_entity_id_key]));
      return $crops;
    }

  }

  /**
   * Sort crop by original image.
   *
   * @param array $crops
   *   Full crop entities keyed by fid.
   */
  public static function sortCropsByOriginal(array &$crops) {
    $sorted = array();
    foreach ($crops as $crop) {
      $sorted[$crop->field_crop_original_image[LANGUAGE_NONE][0]['fid']] = $crop;
    }
    $crops = $sorted;
  }

  /**
   * Save relation table data to connect a crop entity to a node.
   *
   * @param int $referencing_entity_id
   *   ID of the referencing entity.
   * @param string $referencing_entity_type
   *   Type of the referencing entity.
   * @param int $cfid
   *   The crop fid.
   * @param null|array $existing_relations
   *   Existing crop relations from an entity with images on it.
   *
   * @return bool
   *   Whether the relation was saved (TRUE) or not (FALSE).
   *
   * @throws \PDOException
   *   If the crop relation is not saved.
   */
  public static function saveCropRelation($referencing_entity_id, $referencing_entity_type, $cfid, $existing_relations = NULL) {
    if (!empty($existing_relations) && !empty($existing_relations[$cfid])) {
      return FALSE;
    }
    $crop = file_load($cfid);
    try {
      db_insert('crop_entity_usage')
        ->fields(
          array(
            'cfid' => $crop->fid,
            'referencing_entity_id' => $referencing_entity_id,
            'referencing_entity_type' => $referencing_entity_type,
            'ofid' => $crop->field_crop_original_image[LANGUAGE_NONE][0]['fid'],
            'crop_uri' => $crop->uri,
          )
        )
        ->execute();
    }
    catch (\PDOException $error) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Remove an entity from the relations table.
   *
   * @param int $cfid
   *   File ID of the crop entity to remove.
   */
  public static function deleteCropRelation($cfid) {
    db_delete('crop_entity_usage')
      ->condition('cfid', $cfid)
      ->execute();
  }

  /**
   * Build crop entity and render image.
   *
   * @param object $original_image
   *   Original image entity.
   * @param string $ratio
   *   Ratio key as set in the registered ratios.
   * @param int $x
   *   X offset of the crop.
   * @param int $y
   *   Y offset of the crop.
   * @param int $width
   *   Crop int width.
   * @param int $height
   *   Crop height.
   *
   * @return object
   *   A crop file entity.
   */
  public static function buildCropEntity($original_image, $ratio = '', $x = 0, $y = 0, $width = 0, $height = 0) {
    $crop = file_uri_to_object($original_image->uri, FALSE);
    $crop->type = 'crop_entity';

    $values = array(
      'field_x_offset' => $x,
      'field_y_offset' => $y,
      'field_crop_width' => $width,
      'field_crop_height' => $height,
      'field_crop_ratio' => $ratio,
    );

    foreach ($values as $field => $value) {
      CropEntityUtility::setFieldValue($crop, $field, $value);
    }

    CropEntityUtility::setFieldValue($crop, 'field_crop_original_image', $original_image->fid, 'fid');

    return $crop;
  }

  /**
   * Build a crop entity without data.
   *
   * Convenient for subsequent population of the object.
   *
   * @param object $original_image
   *   Full image object to create the crop from.
   *
   * @return object
   *   An empty crop entity with no ID.
   */
  public static function buildEmptyCrop($original_image) {
    $crop = file_uri_to_object($original_image->uri, FALSE);
    $crop->type = 'crop_entity';
    $crop->field_crop_original_image = array(
      LANGUAGE_NONE => array(
        array('fid' => $original_image->fid),
      ),
    );

    return $crop;
  }

  /**
   * Set a field value in a drupal field array.
   *
   * @param object $entity
   *   The entity to update.
   * @param string $field_name
   *   The field to update.
   * @param mixed $value
   *   The value to update the field with.
   * @param string $key
   *   Array key for the value.
   * @param int $delta
   *   The delta for the item to update.
   * @param string $language
   *   Language of the field.
   */
  public static function setFieldValue(&$entity, $field_name, $value, $key = 'value', $delta = 0, $language = LANGUAGE_NONE) {
    $entity->$field_name = array(
      $language => array(
        $delta => array($key => $value),
      ),
    );
  }

  /**
   * Replace the uri for a single image.
   *
   * If there is no crop for the image, it will keep the original URI.
   *
   * @param int $entity_id
   *   ID of the referencing entity.
   * @param string $entity_type
   *   Type of referencing entity.
   * @param object|array $image
   *   Data structure representing the original image entity.
   */
  public static function overrideSingleImage($entity_id, $entity_type, &$image) {
    $crops = CropEntityUtility::loadCropRelations($entity_id, $entity_type, FALSE, 'ofid');
    if (empty($crops[$image['fid']])) {
      return;
    }
    $crop_uri = file_load($crops[$image['fid']]->cfid)->uri;

    if (is_array($image)) {
      $image['uri'] = $crop_uri;
    }
    else {
      $image->uri = $crop_uri;
    }
    return $crops[$image['fid']];
  }

  /**
   * Get supported fields for at given entity bundle.
   *
   * @param string $entity_type
   *   E.g. node, user..
   * @param string $bundle
   *   Article, page or whatever.
   *
   * @return array
   *   List of field names on the given entity that supports cropping.
   */
  public static function getImageFields($entity_type, $bundle) {
    $supported_widgets = crop_entity_supported_widgets();
    $field_info_instances = field_info_instances($entity_type, $bundle);

    $supported_fields = array();

    foreach ($field_info_instances as $field_name => $field) {
      if (in_array($field['widget']['type'], $supported_widgets)) {
        $supported_fields[] = $field_name;
      }
    }
    return $supported_fields;
  }

}
