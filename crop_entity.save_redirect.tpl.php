<?php
/**
 * @file
 * Template for the page the user is redirected to after submitting crop form.
 */
?>
<script>
  var previewId = 'crop-entity-preview-<?php print $fid; ?>';
  var linkId = 'crop-entity-wrapper-<?php print $fid; ?>';
  var editUrl = '/crop_entity/file/<?php print $cfid; ?>';
  var cropFieldId = 'crop-entity-parent-<?php print $fid; ?>';
  window.parent.document.getElementById(previewId).src = "<?php print $url; ?>";
  window.parent.document.getElementById(previewId).removeAttribute("height");
  window.parent.document.getElementById(linkId).href = editUrl;
  var cropField = window.parent.document.getElementById(cropFieldId);
  if (null !== cropField) {
    cropField.value = Math.round(<?php print $cfid; ?>);
    window.parent.CropEntityWidget.dialogDestroy();
  }
  else {
    window.parent.CropEntityWidget.dialogDestroy();
  }
</script>
