Crop Entity
===========

Author: Kristoffer Arrild / kristougher <ka@peytz.dk>

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

## Introduction
Crop Entity allows you to create a context specific crop of an image. When you
crop an image from e.g. a node form, a crop entity will be created and a
relation from the node to the crop will be registered. Thereby you can safely
reuse images from your media library and crop them specific to the content at
hand. It creates a new image in the file system that will replace the original
image when rendering the referencing entity.

## Requirements
The module includes the module crop_entity_file_type. Neither the main nor the
submodule makes sense without the other.

You need to include imgareaselect in your libraries folder (in a folder called
jquery.imgareaselect) http://odyniec.net/projects/imgareaselect/

The jquery version needs to be 1.7 or above. Mainly because the recommended
.on() function is used.

### Dependencies:
features >= 2.0
image >=7.8
jquery_update >=2.0
libraries >=2.1
xautoload >= 7.x-4.0
crop_entity_file_type
 - text
 - number
 - file_entity
 - ctools

The xautoload is not strictly necessary for the functionality, but I kind of
like it, so I decided to use it.

## Recommended modules
 * Markdown filter (https://www.drupal.org/project/markdown):
   When enabled, display of the project's README.md help will be rendered
   with markdown.

## Installation
To use the module, just install it and the known supported widgets will be
processed by Crop Entity and the widget will be modified.

The file type 'crop' is a fieldable entity, so you can add other fields to the
entity and make the editable by hooking into the crop form. This could be a
caption field that should be changeable for each crop.

## Configuration
You do not need to do any configuration for the module to work, but you can
set the size of the crop dialog and the image style used in the preview at
admin/config/media/crop_entity.

Use the available hooks to register new ratios and/or register/unregister
supported widgets.

## Troubleshooting

### The crop widget appears on fields where I dont want it.
Use the hook_crop_entity_supported_widgets_alter() to limit the application
of the crop function to the widgets you want.

### I need a custom crop without a fixed ratio
Use hook_crop_entity_ratio_info() to register a ratio with width and height set
to 0.

## FAQ

### Why another crop module
There are fine crop modules out there, why make another? This started out as an
extension to Manual Crop that was built into a project I was working on.
However, the data model and behavior needed to support the crop per instance in
stead of pr. image required so much modification that backwards compatibility
would be a mess.

### Can I add fields to the crop entity.
Yes, as long as you leave the existing fields, the entity can be expanded as
you like. The form in the crop dialog

## Maintainers
kristougher <ka@peytz.dk>
